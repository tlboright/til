# How to Draw a Filled Square in Canvas

The lineTo() method in canvas context may seem a bit counterintuitive at first.

When using the ctx.lineTo() method, it is important to pay attention to the current position of the cursor that is used to draw the lines that comprise the shape on the screen.

Take the following code for instance:

```js
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
ctx.fillStyle = "skyblue";
ctx.strokeStyle = "black";
ctx.beginPath();
ctx.moveTo(20, 20);
ctx.lineTo(100, 20);
ctx.stroke();
```

We start by beginning the path and moving to x-coordinate 20 and y-coordinate 20 without making any marks.

Then, with the cursor at 20, 20, we draw a line to x-coordinate 100 while the y-coordinate (20) stays the same.

This is a horizontal line of 80 pixels. My understanding of drawing lines broke down here, as I thought that our cursor only moved when using the moveTo action.

In my mistaken understanding, after drawing the line from (20, 20) to (100, 20), the cursor for the drawing would return to (20, 20) and the user would make the next mark from there.

What actually happens is the cursor moves while drawing. After ctx.lineTo(100, 20), our next ctx.lineTo starting point is (100, 20);

To draw a circle that is 80 px wide starting from (20, 20), we can do the following:

```js
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
ctx.fillStyle = "skyblue";
ctx.strokeStyle = "black";
ctx.beginPath();
ctx.moveTo(20, 20);
ctx.lineTo(100, 20);
ctx.lineTo(100, 100);
ctx.lineTo(20, 100);
ctx.lineTo(20, 20);
ctx.stroke();
```

You can add fills or other stylings as you desire.

[Here](https://jsfiddle.net/mnxbc03w/) is a jsfiddle illustrating this concept.

Remember, like a pen that you don't pick up off a piece of paper, when you use consecutive ctx.lineTo() method calls, the next call you make to lineTo() starts from the (x, y) coordinates of the previous lineTo call.
